/**
 * Created by Gilberto on 11/20/2015.
 */
public class Comment {
    String content;
    int upvotes;
    int id;
    Comment(String content,int upvotes,int id){
        this.content=content;
        this.upvotes=upvotes;
        this.id=id;
    }

    public String toString(){
        return content+" Upvotes: "+upvotes+" Id: "+id;
    }
}
