import java.util.*;
import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException{
	    Scanner scan=new Scanner(new File("table.txt"));
        int id=scan.nextInt();
        int parentId=scan.nextInt();
        int upvotes=scan.nextInt();
        String content=scan.nextLine().trim();
        Comment c=new Comment(content,upvotes,id);
        Node<Comment> root=new Node<Comment>(c);
        while(scan.hasNextLine()){
            id=scan.nextInt();
            parentId=scan.nextInt();
            upvotes=scan.nextInt();
            content=scan.nextLine().trim();
            c=new Comment(content,upvotes,id);
            try{
                findNode(root,parentId).addChild(new Node<Comment>(c));
            }catch (Exception e){
                System.out.println("Error:"+parentId+" id:"+id);
            }

        }

        displayPostOrder(root);

    }
    public static void displayPostOrder(Node<Comment> node){
        System.out.println(node.getData());
        List<Node<Comment>> children=node.getChildren();
        for(Node<Comment> temp:children){
            displayPostOrder(temp,"     ");
        }

    }
    public static void displayPostOrder(Node<Comment> node,String tab){
        System.out.println(tab+node.getData());
        List<Node<Comment>> children=node.getChildren();
        for(Node<Comment> temp:children){
            displayPostOrder(temp,tab+"     ");
        }

    }
    public static Node<Comment> findNode(Node<Comment> node,int id){
        if(node.getData().id==id){
            return node;
        }else{
            List<Node<Comment>> list=node.getChildren();
            for(Node<Comment> temp:list){
                Node<Comment> n= findNode(temp,id);
                if(n!=null){
                    return n;
                }
            }

        }
        return null;
    }
}
